/*Global Variables*/
var Winwidth, Winheight, DeviceType, CurrentDevice, MobileMenu, ResizeTimer, CurrentBreakpointm;

var ezone = {} || ezone;

winWidth = window.innerWidth;

if (typeof ezone.commonFunctions === 'undefined') {
	var commonFunctions = {};
}

(function ($) {

	"use strict";

	ezone.commonFunctions = {
		
		
		UserAgentCode : function () {
			// detect touch
			if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) 
				|| navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)
				|| navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/)
				|| navigator.userAgent.match(/Windows Phone/i) || navigator.userAgent.match(/ZuneWP7/i)) {
				$('body').addClass('touch-screen');
			}
		},
		
		hemMenu : function() {
			 $(".hamburger").on('click', function(e){ 
	                 e.preventDefault();
	                 $(this).toggleClass("open");
	                 $('body').toggleClass('offcanvas');
	                 $('#header-navigation').slideToggle();
	             });
		},

		SliderAnimations : function () {
			
			
			$('.home-slider').slick({
				autoplay: true,
				autoplaySpeed: 8000,
				arrows: true,
				dots: false,
				speed: 1000,
				slidesToShow: 3,
				slidesToScroll: 1,
				responsive: [
					{
					  breakpoint: 1024,
					  settings: {
						slidesToShow: 3,
						slidesToScroll: 1,
						infinite: true
					  }
					},
					{
					  breakpoint: 600,
					  settings: {
						slidesToShow: 2,
						slidesToScroll: 1
					  }
					},
					{
					  breakpoint: 480,
					  settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					  }
					}
				]
			});
			
		},
	
		
		orientation : function () {
			Timer : false,
				clearTimeout(this.Timer);
				this.Timer = setTimeout(function () {
					Winwidth = window.innerWidth;
					Winheight = window.innerHeight;
					CurrentBreakpoint : 'Desktop',
					DeviceType = {
						DESKTOP : 'Desktop',
						MOBILE : 'Mobile'
					};
					CurrentDevice = DeviceType.DESKTOP;

					if (Winwidth >= 992) {
						CurrentDevice = DeviceType.DESKTOP;
						/*} else if (Winwidth > 990 && Winwidth < 1024) {
						CurrentDevice = DeviceType.TABLET;*/
					} else if (Winwidth < 992) {
						CurrentDevice = DeviceType.MOBILE;
					}

					ezone.commonFunctions.CurrentBreakpoint = CurrentDevice;

					console.log(CurrentDevice);
				}, 10);
		},

		init : function () {	
			ezone.commonFunctions.orientation();
			ezone.commonFunctions.UserAgentCode();
			ezone.commonFunctions.SliderAnimations();
			ezone.commonFunctions.hemMenu();
		}
	}

	$(window).on('resize', function () {
		ezone.commonFunctions.orientation();
	});

	$(document).ready(function () {		
		ezone.commonFunctions.init();		
	});

	$(window).scroll(function () {
	
	});
	

}).call(ezone.commonFunctions, window.jQuery);
